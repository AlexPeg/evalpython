from django.contrib import admin

# Register your models here.
from .models import Formateur,Note,Student
admin.site.register(Formateur)
admin.site.register(Note)
admin.site.register(Student)

