from django.http import JsonResponse
from django.shortcuts import render

from .models import Student


def serialize_note(note):

    students = []
    for student in note.students.all():
        students.append(str(student))

    return {
        "id": note.id,
        "author": str(note.author),
        "subjet": note.subjet,
        "notes": note.notes,
        "date": note.date,
        "students": students,
    }


def accueil(request):
    return render(request, "eval/accueil.html")


def notes_by_student(request):
    students = Student.objects.all()
    context = {
        "students": students,
    }
    return render(request, "eval/notes_by_student.html", context)


def get_notes_by_student(request, student_id):
    student = Student.objects.get(pk=student_id)
    student_notes = student.note_set

    data = []

    for note in student_notes.all():
        data.append(serialize_note(note))

    return JsonResponse(data, safe=False)
